from random import choice


class Lotto:
    def __init__(self, lotto_type=None):
        if not lotto_type:
            self.reset()
        self.lotto_type = lotto_type
        self.max_number = 45 if lotto_type == 6 else 39
        self.numbers = []
        for i in range(self.max_number):
            self.numbers.append(i + 1)
        self.game_numbers = self.game_nums()
        self.player_numbers = self.player_nums()

    def game_nums(self):
        nums = []
        while len(nums) < self.lotto_type:
            num = choice(self.numbers)
            if num not in nums:
                nums.append(num)
        return nums

    def player_nums(self):
        nums = []
        while len(nums) < self.lotto_type:
            num = input(f"Enter num #{len(nums)+1}: ")
            try:
                num = int(num)
            except Exception:
                print("Invalid number!")
                continue
            if num < 1 or num > self.max_number:
                print("Invalid number!")
                continue
            if num in nums:
                print("Number already picked!")
                continue
            nums.append(num)
        return nums

    def check_hits(self):
        counter = 0
        for x in self.player_numbers:
            if x in self.game_numbers:
                counter += 1
        return counter

    def play(self):
        hits = self.check_hits()
        if hits != self.lotto_type:
            print(f"No lotto :( You had {hits}/{self.lotto_type} hits")
        else:
            print("Lotto!!!")
        print(f"Your numbers: {', '.join([str(x) for x in self.player_numbers])}")
        print(f"Game numbers: {', '.join([str(x) for x in self.game_numbers])}")
        self.reset()

    @staticmethod
    def reset():
        num = 0
        while num not in [6, 7]:
            num = input("Enter game type: ")
            try:
                num = int(num)
            except Exception:
                print("Invalid game type!")
                continue
            if num not in [6, 7]:
                print("Invalid game type!")
                continue
        game = Lotto(num)
        game.play()


try:
    lotto = Lotto()
except KeyboardInterrupt:
    print("")
    print("Bye")
